
# 1.除0异常 ZeroDivisionError
# 1/0


# 2.名称异常 NameError
# print(name)

# 3.类型异常 TypeError
# print('1' + 2)

# # 4.索引异常 IndexError
# l = [1, 2]
# l[3]

# # 5.键异常 KeyError
# dict = {"name": "子木"}
# dict["lz"]

# # 6.值异常异常 ValueError
# int('abc')

# 7.属性异常  AttributeError
# class Person(object):
#     pass
#
# p = Person()
# print(p.name)

# # 8.迭代器异常 StopIteration
it = iter([1, 2])
print(next(it))
print(next(it))
print(next(it))


# 总结 :系统异常继承树 特定异常 --> Exception -- > BaseException -- > object

#  捕捉异常
#



