# class Animal(object):
#     pass
#
# class xxxx(object):
#     pass
#
# # 单继承
# class Dog(Animal):
#     pass
#
# # 多继承
# class Dogg(Animal,xxxx):
#     pass
#
# # 查看当前类的父类
# print(Dogg.__bases__)
# print(int.__bases__)
# print(float.__bases__)
# print(str.__bases__)   # object类
# print(bool.__bases__)    # int类
# print(type.__bases__)   # 继承object类
# print(object.__class__)  # object 由 type 创建 没有继承 type 继承了object 同时创建了object


# -------------------1.继承类之后的访问权限------------------
# class Anmail(object):
#
#     # 属性
#     a = 1
#     _b = 2
#     __c = 3
#
#     def t1(self):
#         print("公有t1，Anmail")
#
#     def _t2(self):
#         print("受保护的t2，Anmail")
#
#     def __t3(self):
#         print("私有的t3，Anmail")
#
#     def __init__(self):
#         print("内置的方法，Anmail")
#
#
# class Dog(Anmail):
#     def test(self):
#         print(self.a)
#         print(self._b)
#         # print(self.__c)
#
#         self.t1()
#         self._t2()
#         # self.__t3()
#
#         self.__init__()
#
#
# d = Dog()
# d.test()
# 私有方法子类不可以访问，受保护、公有、内置都可以使用
# -------------------2.继承原则 - MRO - 深度优先  后来是C3算法 python3.0 之后都是C3算法 ------------------
# 单继承 ： 按照继承链条往上查找
# 无重叠的多继承：按照单一链条查找之后，再找另一条链条
# 有重叠的多继承：从下往上查找 A ->B -> C - >D

import inspect

class E:
    age = "e"

class D:
    age = "d"

    def __init__(self):
        self.d_age = 10


class C(E):
    # age = "d"
    def test(self):
        print(self)

    @classmethod
    def test2(cls):
        print(cls)

    def __init__(self):
        self.c_age = 10
        super(C, self).__init__()


class B(D):
    age = "b"
    def __init__(self):
        self.b_age = 10
        super(B, self).__init__()


class A(C, B):   #从继承左侧开始查找链
    # age = "a"

    def __init__(self):
        self.a_age = 10
        # super(type, obj) 不是父类关系 是MRO 下一级链条
        super(A, self).__init__()  #也可以super().__init__() python3.0之后

print(inspect.getmro(A))
A.test2()
A().test()
print(A().__dict__)
