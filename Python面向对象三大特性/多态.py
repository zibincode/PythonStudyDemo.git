# class Animal(object):
#     def jiao(self):
#         pass
#
# class Dog(Animal):
#     def jiao(self):
#         print('汪汪汪')
#
# class Cat(Animal):
#     def jiao(self):
#         print("喵喵喵")
#
#
# def test(obj):
#     obj.jiao()
#
# d = Dog()
# c = Cat()
# test(c)

#  python 是动态语言 不需要管类型  只需要管是否有这个方法、和属性  所以python没有真正意义的多态、也不需要

# ----------------------------------抽象类和抽象方法-------------------------------------

# import  abc
#
# # 主要借助abc 模块    metaclass 设置为  abc.ABCMeta 抽象类不能直接使用 必须实现抽象方法
# class Animal(object, metaclass=abc.ABCMeta):
#     @abc.abstractmethod
#     def jiao(self):
#         pass
#
# class Dog(Animal):
#     def jiao(self):
#         print('汪汪汪')
#
# class Cat(Animal):
#     def jiao(self):
#         print("喵喵喵")
#
#
# d = Dog()
# d.jiao()

# ------------------------三大特性的案例--------------------

class Animal(object):
    def __init__(self, name, age=1):

        self.age = age

        self.name = name

    def eat(self):
        print('{}在吃饭'.format(self))

    def play(self):
        print('{}在玩耍'.format(self))

    def sleep(self):
        print('{}在睡觉'.format(self))

    def work(self):
        pass

class Person(Animal):

    def __init__(self, name, pets, age=1):

        self.pets = pets

        super(Person, self).__init__(name, age)

    # 养宠物
    def yang_pet(self):

        for object in self.pets:
            object.eat()
            object.play()
            object.sleep()

    # 让宠物工作
    def make_pets_Work(self):

        for pet in self.pets:

            pet.work()

    # 类的描述
    def __str__(self):
        return '名字是{}，年龄是{}岁的人'.format(self.name, self.age)

class Dog(Animal):

    # 类的描述
    def __str__(self):
        return '名字是{}，年龄是{}岁的小狗'.format(self.name, self.age)

    def work(self):
        print('{}在看家'.format(self))


class Cat(Animal):
    def work(self):
        print('{}在捉老鼠'.format(self))

    # 类的描述
    def __str__(self):
        return '名字是{}，年龄是{}岁的小猫'.format(self.name, self.age)

d = Dog('招财', 3)

c = Cat("进宝", 4)

p = Person('子彬',[d, c], 27)

p.yang_pet()

p.make_pets_Work()

p.eat()



