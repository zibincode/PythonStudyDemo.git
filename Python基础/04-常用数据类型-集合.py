# -*- coding:utf-8 -*-
# 1.定义  无序的 不可随机访问，不可重复  可以有交集 并集 补集
# s = {1, 2, 3, 4, 3}
# print(s)

# 2. 可变集合 set  可以增、删除、修改   frozenset不可变集合 不可以改变
# # 可变集合定义方法一
# s = {435, 45, 5}
# print(s, type(s))

# 可变集合定义方法二 set(Iterable)
# s = set([34, 56, 7])
# print(s, type(s))

# 可变集合定义方法三 集合推导式
s = set(x * x for x in range(0, 4))
print(s, type(s))

# 不可变集合定义方式一 frozenset(Iterable)
s1 = frozenset((1, 3, 5))
s1 = frozenset([1, 3, 5])
s1 = frozenset({'name': 'lzb', 'age': 18})
# print(s1, type(s1))

# 不可变集合定义方式二 集合推导式
# s = frozenset(x * x for x in range(0, 6))
# print(s)

# 数组是可变的  元祖是不可变的    字典是可变的  集合新增的数必须是可哈希的（不可变的）
# 集合可以帮助列表去重 - 不可重复
list1 = [1, 3, 4, 5, 6, 2, 1]
set1 = set(list1)
result = list(set1)
print(result)

# 3. 集合常用操作之单一集合的操作 增、删、改、查
# s = {1, 2, 3}
# s.add(4)
# print(s, type(s))
# 删除
# # remove  没有这个元素  报错
# print(s.remove(12))
#
# # discard    没有这个元素  不报错 返回 None
# print(s.discard(12))
#
# # pop   随机删除一个元素  空集合删除会报错
# print(s.pop())
# print(s.pop())
# print(s.pop())
# print(s.pop())
#
#
# # clear  清空集合  返回 None
# print(s.clear())

# 集合 不可以修改

# 集合的遍历 方式一 for in
for v in s:
    print(v)

# 集合的遍历 方式二 迭代器
its = iter(s)
print(next(its))
print(next(its))
print(next(its))

# its = iter(s)
# for v in its:
#     print(v)

# 4. 集合之间的操作 交集 并集 补集

# 4.1  intersection(Iterable)  求交集    可变与不可变交集时候 以intersection左侧为基准
# # intersection_update 求交集  只有不可变才有  会修改调用者
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# result = s1.intersection(s2)
# print(result, type(result))
#
# s1 = frozenset([1, 2, 3, 4, 5])
# s2 = {4, 5, 6}
# result = s2 & s1
# print(result, type(result))
#
# s1 = frozenset([1, 2, 3, 4, 5])
# s2 = {4, 5, 6}
# s2.intersection_update(s1)
# print(s2, type(s2))
#
# s1 = {1, 2, 3, 4, 5}
# print(s1.intersection([1, 2, 3]))

# 4.2  union(Iterable)  求并集
# # update 求交集  只有不可变才有  会修改调用者
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# print(s1.union(s2))
#
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# print(s1 | s2)

# 4.3  difference(Iterable)  求补集 差集
# difference_update  求补集 差集  只有不可变才有  会修改调用者
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# print(s1.difference(s2))
#
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# print(s1 - s2)

# 4.4  判断 判断集合是否包含
# isdisjoint 是否不相交
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5, 6}
# print(s1.isdisjoint(s2))
#
# # issuperset 是否全包含  s1 包含 s2
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5}
# print(s1.issuperset(s2))
#
# # issuperset 是否全包含  s2 是 s1 的子集
# s1 = {1, 2, 3, 4, 5}
# s2 = {4, 5}
# print(s2.issubset(s1))







