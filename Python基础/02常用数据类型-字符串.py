# -*- coding:utf-8 -*-
# name = "我是刘子彬'liuzibin'"
# print(name)

# name = '''
#  我是
#  刘子彬你
#  你是谁
# '''
# print(name)

# 1.字符串的拼接
# strName = "我是刘子彬" + "您是谁啊"
# print(strName)

# 模板字符串
# strName = "我是刘子彬%s,%d"%("您是谁啊", 2300)
# print(strName)

# 2.字符串的切片[start : end : step]
name = "abcedefbhgth"
# print(name[0:len(name):2])
# print(name.find('b', 3))

# print(name.capitalize())

# print(name.replace('b', 'B'))
name = "liuzibin-sex-age-score-0830-1234567"
print(name.split('-'))


