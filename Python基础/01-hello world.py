# print(1)
# print(2)
# print(3)
# print("打印中文--我是刘子彬 --hello world")

# python的数据类型

# print(9 + 9)
# print('9' + '9')

# 常用数据类型
# Numbers (数值类型)
# print(2332)
# print(1.2)
# print(34532542354235)


# bool (布尔类型)
# print(True)
# print(False)



# String (字符串)
# print('aaaaa')
# print("aaaaaa")
# print('''aaaaaaaa''')


# list (列表）
# set (集合)
# tuple (元祖)
# dict (字典)
# NoneType (空类型)

# 查看数据类型 type()

# num = 56
# result = type(num)
# print(result)


# 数据类型的转换
# num = 4
# print(str(num) + '6')
# print(num + int('6'))

# num = input('请输入一个数字\n')
# print(type(num))
# print(int(num) * 2)

# Python 是动态类型 的强语言







