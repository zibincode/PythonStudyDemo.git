'''

1、 一个.py文件就是一个模块 （当个文件）

2 、包含一个__init__.py就是一个包 （目录文件夹）

   __init__.py 的作用：导入包会执行这个文件

3、 包和模块的分类

    3.1 标准的包和模块 -> 系统安装好
    3.2 第三方包和模型 -> 别人开发的
    3.2 自定义包和模型 -> 自己开发的
    
 导入的方式


 导入的底层原理
 第一次导入 ：
 1.执行被导入模块所有的代码；
 2.创建一个模块对象，并将模块内所有顶级变量以属性的方式加载模块对象上面
 3.在import 的位置，引入import 后面的变量名称到当前的命名空间 （多次导入只是执行这个步骤）



 导入的检索路径
 第一次导入：
  1. 先从内置模块中找 - 没有找到找第二
  2.在从sys.path中找  sys.path 是一个数组

  第二次导入： 重已经加载中的模块去找 查看已经加载模块 sys.modules


  sys.path 包括哪些路劲：
  1. 当前目录  运行那个文件就是以那个文件为目录， 特别注意子模块参考的路径也是这个
  2. 环境变量PYTHONPATH指定的路径列表
  3. .pth文件中的文件路径列表
  4. python安装路径中的lib库中搜索

 追加路径到sys.path方式：
 1. 直接增加到sys.path 列表的后面
 2. 修改PYTHON对应的环境变量文件路径
 3. 增加一个.pth文件， 在文件增加文件路径 （在安装路径或者site-packages路径下面增加）




 

'''


# import package01.A_moudle as  moudle_A
# import package01.p1.Tool as tool
#
# # print(moudle_A)
#
# print(tool.Tool.run())
# tool.commonRun()


# from package01.p1 import Tool as tool, Tool2 as tool2
# tool.commonRun()
# print(tool2.name)
# from package01.p1 import *
# Tool.commonRun()
# print(Tool2.num)


# 文件加载优先级的路径 是列表 可以修改
# import sys
# print(sys.path)
# print(sys.modules)


# # 当导入A 失败的时候 导入B   导入可选 优先导入A
# try:
#     import A as  a
# except ModuleNotFoundError:
#     import B as a
