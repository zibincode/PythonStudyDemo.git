

# class Tool(object):
#
#     @staticmethod
#     def tool_sum(a, b):
#         return a + b
# print(Tool.tool_sum(1, 2))

"""
# 概念理解

1、 一个.py文件就是一个模块 （当个文件）

2 、包含一个__init__.py就是一个包 （目录文件夹）

   __init__.py 的作用：导入包会执行这个文件

3、 包和模块的分类

    3.1 标准的包和模块 -> 系统安装好
    3.2 第三方包和模型 -> 别人开发的
    3.2 自定义包和模型 -> 自己开发的
    

"""

# 3.1 标准的包和模型 -> 系统安装好,内置的包和模块(builtins)

# import  os
# print(os.path)


# 查看模块的内容
# import builtins
# print(dir(builtins))



