'''
 本地安装：
  方式一：对带有setup.py的文件安装
     步骤一：打卡命令工具
     步骤二： cd 到下载包的setup.py 目录
     步骤三： 根据不同的环境之下  python3 setup.py install

  方式二： 对带有.egg文件的包安装
       使用setuptools自带脚本easy_install 命令： easy_install xxx包文件名称

  方式三： .whl文件的包的安装
       1. easy_install  安装
       2. pip 安装

  远程安装pip
 1.不同python环境安装
  python  -m pip install xxxx
  python3 -m pip install xxxx

 2. 查看包
 pip -h  帮助文档
 pip list
 pip show xxxx 包的信息

 3.  搜索包
 pip search  xxx

 4. 安装特定的版本
 pip install SomePackage            # latest version
 pip install SomePackage==1.0.4     # specific version
 pip install 'SomePackage>=1.0.4'     # minimum version

 5. 升级包
 pip install  --upgrade  xxx

 6. 卸载包
 pip uninstall xxx

 7. 冻结第三包  生成第三包名称文件列表
 pip freeze

 需要安装的文件
 pip install -r 文件名称

 pip freeze > requirements.txt
 pip install -r requirements.txt

 8.第三方包版本规则

 9. pip 安装源问题 一般pip在国外比较慢  可以采用国内pip源
    -- 豆瓣：https://pypi.douban.com/simple  (推荐)
    -- 阿里：https://mirrors.aliyun.com/pypi/simple
   pip install -i https://pypi.douban.com/simple 模块名

   永久安装pip源
   mac 系统
   1. cmd 的根路径 创建.pip 文件夹   通过ls -a 可以查看文件
        mkdir .pip
   2. 进入.pip 文件夹 创建pip.conf配置文件
        touch pip.conf
   3. 文件输入
     """
    [global]   # 全局有效
    index-url = http://pypi.douban.com/simple  # 索引地址

    [install]
    use-mirrors =true
    mirrors = http://pypi.douban.com/simple/
    trusted-host = pypi.douban.com   # 信任主机名

    """

    windows 系统
    1. 用户根目录新建 pip 文件夹并在文件夹中
    2. 新建 pip.ini 配置文件
    3. 文件输入内容一致


    10. 发布自己的包
    第一步 注册账号  https://pypi.org/
        账号：lzb
        邮箱：1835064412@qq.com

    第二步 环境准备  4个环境
         1.setuptools 环境安装  打包成egg格式
         2.pip 安装
         3.wheel 安装    打包成wheel
         4.twine 安装  发布包使用上传包到pypi平台
    第三步 发布前准备 tar.gz / zip /.egg /.whl 文件
           必须有setup.py文件，配置setup.py的文件

    第四步 执行命令生成发布包
          # 查看打包命令
           python3 setup.py --help-commands

          # 打包
          python3 setup.py sdist 源码压缩包      生成dist文件
          python3 setup.py bdist 二进制发布包   结果不包括setup.py的二进制文件

          python3 setup.py  sdist --formats=zip,tar  压缩成成不同格式的源文件
          python3 setup.py bdist_egg       .egg格式
          python3 setup.py bdist_wheel     .whl格式
          python3 setup.py bdist_wininst   windows下面的文件exe

        一般打包（待源码的sdist, 一个是二进制 .whl 文件）

  第四步 发布包的安装
      pip install 包名  针对.egg  .whl
      easy_install  包名  针对.egg  .whl

  第五部 上传包 借助工作twine
      twine upload 包名称













'''