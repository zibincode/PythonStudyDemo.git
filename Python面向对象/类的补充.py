# 1.元类
# 1.1 对象是类创建的   类是由元类创建
# num = 10
# print(num.__class__)  # <class 'int'>
# print(num.__class__.__class__)   #<class 'type'>

# s = 'dsafasdf'
# print(s.__class__)  # <class 'str'>
# print(s.__class__.__class__)   #<class 'type'>

# class Person:
#     pass
#
# p = Person()
# print(p.__class__)   # <class '__main__.Person'>
# print(Person.__class__)  # <class 'type'>


# 2.类对象的创建方式

#  方式一
# class Person:
#     pass

# 方式二 元类创建
# def run(self):
#     print(self)
# d = type("Dog", (), dict(num=10, age='5', run=run))
# print(d, d.__dict__)  # <class '__main__.Dog'>
# dd = d()
# dd.run()  # <__main__.Dog object at 0x10fb16278>

# 3. 类的创建流程

# 4.通过内置的type元类创建
# type
#
# 3. 检测模块中是否有__metaclass__
# __metaclass__ = dddd
#
# 2.检测父类中是否有__metaclass__
# class Animal:
#     pass
# 1.检测当前中是否有__metaclass__
# class Person(Animal):
#     #__metaclass__ = xxx
#     pass


# 4.类的描述
class Person:
    '''
    关于这个类的描述：
    类的作用、类的构造函数
    类属性作用

    Attributes:
        count : int类型  默认值是1 表示人的个数
    '''

    # 这个表示人数
    count = 1

    def run(self, distance, sep = 1):
        '''
        这个方法的作用
        :param distance:  参数的含义 参数的类型  默认值
        :param sep:
        :return:
        '''

        print('人在跑')
# 获取类的描述文档
# help(Person)
"""
class Person(builtins.object)
 |  关于这个类的描述：
 |  类的作用、类的构造函数
 |  类属性作用

 |  Attributes:
 |      count : int类型  默认值是1 表示人的个数
 |  
 |  Methods defined here:
 |  
 |  run(self, distance, sep=1)
 |      这个方法的作用
 |      :param distance:  参数的含义 参数的类型  默认值
 |      :param sep:
 |      :return:
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
 |  
 |  ----------------------------------------------------------------------
 |  Data and other attributes defined here:
 |  
 |  count = 1
 
"""

# 5. 生成项目文档

# 方式一 ：python内置模块，使用pydoc模块

# 终端查看：终端使用python3 -m pydoc 模块名称

# 网页查看html：终端使用python3 -m pydoc -p (或者-b)

# 本地生成html：终端使用python3 -m pydoc -w

# 方式二 ：使用第三方模块生成 sphinx epydoc















