# ---------------1.监听python对象的生命周期----------------
# class Person(object):
#    # 1.可以拦截对象的创建
#    def __new__(cls, *args, **kwargs):
#        print('__new__方法调用')
#        return super(Person, cls).__new__(cls, *args, **kwargs)
#
#    # 2.创建对象完成后会自动调用这个方法，并把实例传递给init方法
#    def __init__(self):
#        print('__init__初始化方法')
#        self.name = 'zb'
#
#    # 3.对象释放的时候自动调用
#    def __del__(self):
#        print('__del__对象释放')
#
# p = Person()
# # print(p)
# del p


# ----------------------2.案例：要求计算某一个时刻由Person类产生实例的个数


# class Person(object):
#
#     # 类属性
#     __personCount = 0
#
#     def __init__(self):
#         print('计数 + 1')
#         Person.__personCount += 1
#
#     def __del__(self):
#         Person.__personCount -= 1
#         print('计数 - 1')
#
#     @classmethod
#     def log(cls):
#         print('当前创建person的个数：%d' %(cls.__personCount))
#
#
# p = Person()
# p2 = Person()
# del p
#
# Person.log()

