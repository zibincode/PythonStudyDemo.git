# 案例一：实现一个计算器，实现加减乘除的基本运算并打印结果
# ---------------------------------代码1-------------------------
# 计算 （3 + 6 - 2）*4

# result = 0
#
# def firstValue(v):
#     global result
#     result = v
#
# def jiafa(n):
#     global result
#     result = result + n
#
# def jianfa(n):
#     global result
#     result = result - n
#
# def chengfa(n):
#     global result
#     result = result * n
#
# firstValue(3)
# jiafa(6)
# jianfa(2)
# chengfa(4)
# print(result)

# ---------------------------------代码2:优化-------------------------
# class caculator(object):
#
#     __result = 0
#     def firstValue(cls, v):
#         cls.__result = v
#
#     @classmethod
#     def jiafa(cls, n):
#         cls.__result = cls.__result + n
#
#     @classmethod
#     def jianfa(cls, n):
#         cls.__result = cls.__result - n
#
#     @classmethod
#     def chengfa(cls, n):
#         cls.__result = cls.__result * n
#
#     @classmethod
#     def showResult(cls):
#         print('计算结果是：%s'%(cls.__result))
#
# caculator.firstValue(3)
# caculator.jiafa(6)
# caculator.jianfa(2)
# caculator.chengfa(4)
# caculator.showResult()

#
# # ---------------------------------代码3:优化-------------------------
# class caculator(object):
#
#     def __init__(self, num):
#         self.__result = num
#
#     def jiafa(self, n):
#         self.__result = self.__result + n
#
#     def jianfa(self, n):
#         self.__result = self.__result - n
#
#     def chengfa(self, n):
#         self.__result = self.__result * n
#
#     def showResult(self):
#         print('计算结果是：%s'%(self.__result))
#
# c = caculator(3)
# c.jiafa(6)
# c.jianfa(2)
# c.chengfa(4)
# c.showResult()


# # ---------------------------------代码4:优化-------------------------
# class caculator(object):
#
#     def __init__(self, num):
#         self.checkType(num)
#         self.__result = num
#
#     def jiafa(self, n):
#         self.checkType(n)
#         self.__result = self.__result + n
#
#     def jianfa(self, n):
#         self.checkType(n)
#         self.__result = self.__result - n
#
#     def chengfa(self, n):
#         self.checkType(n)
#         self.__result = self.__result * n
#
#     def showResult(self):
#         print('计算结果是：%s'%(self.__result))
#
#
#     def checkType(self, typeNum):
#         if not isinstance(typeNum, int):
#             raise TypeError('当前类型不对，应该是整形数据')
#
#
# c = caculator(3)
# c.jiafa(6)
# c.jianfa(2)
# c.chengfa(4)
# c.showResult()


# # ---------------------------------代码5:优化-------------------------
# class caculator(object):
#
#     def checkType(self, typeNum):
#         if not isinstance(typeNum, int):
#             raise TypeError('当前类型不对，应该是整形数据')
#
#     # 装饰器
#     def __check_number_zsq(func):
#         def inner(self, typeNum):
#             if not isinstance(typeNum, int):
#                 raise TypeError('当前类型不对，应该是整形数据')
#             return func(self, typeNum)
#
#         return inner
#
#
#     @__check_number_zsq
#     def __init__(self, num):
#         self.__result = num
#
#     @__check_number_zsq
#     def jiafa(self, n):
#         self.__result = self.__result + n
#
#     @__check_number_zsq
#     def jianfa(self, n):
#         self.checkType(n)
#         self.__result = self.__result - n
#
#     @__check_number_zsq
#     def chengfa(self, n):
#         self.checkType(n)
#         self.__result = self.__result * n
#
#     def showResult(self):
#         print('计算结果是：%s'%(self.__result))
# c = caculator(3)
# c.jiafa(6)
# c.jianfa(2)
# c.chengfa(4)
# c.showResult()


# # ---------------------------------代码6:优化 增加语音播报器-------------------------
#
# class caculator(object):
#
#     # 装饰器2
#     def __add_voice_play_zsq(func):
#         def inner(self, word):
#             print('模拟语音播报：%d' % (word))
#             return func(self, word)
#
#         return inner
#
#
#     # 装饰器1
#     def __check_number_zsq(func):
#         def inner(self, typeNum):
#             if not isinstance(typeNum, int):
#                 raise TypeError('当前类型不对，应该是整形数据')
#             else:
#                 print('验证成功：%s'%(typeNum))
#             return func(self, typeNum)
#
#         return inner
#
#     @__check_number_zsq
#     @__add_voice_play_zsq
#     def __init__(self, num):
#         self.__result = num
#
#     @__check_number_zsq
#     @__add_voice_play_zsq
#     def jiafa(self, n):
#         self.__result = self.__result + n
#
#     @__check_number_zsq
#     @__add_voice_play_zsq
#     def jianfa(self, n):
#         self.__result = self.__result - n
#
#     @__check_number_zsq
#     @__add_voice_play_zsq
#     def chengfa(self, n):
#         self.__result = self.__result * n
#
#     def showResult(self):
#         print('计算结果是：%s'%(self.__result))
# c = caculator(3)
# c.jiafa(6)
# c.jianfa(2)
# c.chengfa(4)
# c.showResult()


# # ---------------------------------代码6:优化 增加语音播报器 播报操作-------------------------
#
# class caculator(object):
#
#     #创建装饰器
#     def __create_voice_play_zsq(opertion = ""):
#         def __voice_play_zsq(func):
#             def inner(self, word):
#                 string = opertion + str(word)
#                 self.__speak_word(string)
#                 return func(self, word)
#
#             return inner
#         return __voice_play_zsq
#
#
#
#     #播放语音函数
#     def __speak_word(self, word):
#         print('模拟语音播报：%s' %(word))
#
#     # 装饰器2
#     def __add_voice_play_zsq(func):
#         def inner(self, word):
#             self.__speak_word(word)
#             return func(self, word)
#         return inner
#
#
#     # 装饰器1
#     def __check_number_zsq(func):
#         def inner(self, typeNum):
#             if not isinstance(typeNum, int):
#                 raise TypeError('当前类型不对，应该是整形数据')
#             else:
#                 print('验证成功：%s'%(typeNum))
#             return func(self, typeNum)
#
#         return inner
#
#     @__check_number_zsq
#     @__create_voice_play_zsq()
#     def __init__(self, num):
#         self.__result = num
#
#     @__check_number_zsq
#     @__create_voice_play_zsq("加")
#     def jiafa(self, n):
#         self.__result = self.__result + n
#
#     @__check_number_zsq
#     @__create_voice_play_zsq("减")
#     def jianfa(self, n):
#         self.__result = self.__result - n
#
#     @__check_number_zsq
#     @__create_voice_play_zsq("乘以")
#     def chengfa(self, n):
#         self.__result = self.__result * n
#
#     def showResult(self):
#         self.__speak_word(self.__result)
#         print('计算结果是：%s'%(self.__result))
#
#     # 描述器
#     @property
#     def result(self):
#          return self.__result
#
#
#
# c = caculator(3)
# c.jiafa(6)
# c.jianfa(2)
# c.chengfa(4)
# c.showResult()
#
# print(c.result)


# ---------------------------------代码7:优化 方法返回self本身 点语法(链式编程)-------------------------
class caculator(object):

    #创建装饰器
    def __create_voice_play_zsq(opertion = ""):
        def __voice_play_zsq(func):
            def inner(self, word):
                string = opertion + str(word)
                self.__speak_word(string)
                return func(self, word)

            return inner
        return __voice_play_zsq



    #播放语音函数
    def __speak_word(self, word):
        print('模拟语音播报：%s' %(word))

    # 装饰器2
    def __add_voice_play_zsq(func):
        def inner(self, word):
            self.__speak_word(word)
            return func(self, word)
        return inner


    # 装饰器1
    def __check_number_zsq(func):
        def inner(self, typeNum):
            if not isinstance(typeNum, int):
                raise TypeError('当前类型不对，应该是整形数据')
            else:
                print('验证成功：%s'%(typeNum))
            return func(self, typeNum)

        return inner

    @__check_number_zsq
    @__create_voice_play_zsq()
    def __init__(self, num):
        self.__result = num

    @__check_number_zsq
    @__create_voice_play_zsq("加")
    def jiafa(self, n):
        self.__result = self.__result + n
        return self

    @__check_number_zsq
    @__create_voice_play_zsq("减")
    def jianfa(self, n):
        self.__result = self.__result - n
        return self

    @__check_number_zsq
    @__create_voice_play_zsq("乘以")
    def chengfa(self, n):
        self.__result = self.__result * n
        return self

    def showResult(self):
        self.__speak_word(self.__result)
        print('计算结果是：%s'%(self.__result))
        return self

    def clear(self):
        self.__result = 0
        return self

    # 描述器
    @property
    def result(self):
        return self.__result

c = caculator(3)
c.jiafa(6).jianfa(2).chengfa(4).showResult()


print(c.result)

c.clear()
