# def fun():
#     pass

# 1.类的定义 类名首字母需要大写
# 经典类 + 新式类
class Money:
    pass


# 2. 根据类实例化对象

one = Money()
print(one)
print(one.__class__ , Money.__name__)  #查看当前类


# 3. 类属性： __name__   实例属性： __class__

# 4. 对象属性

# 4.1 增加属性 实例.属性名 = 值
# class Person:
#     pass
#
# p = Person()
# p.age = 15
# p.name = '刘子彬'
# p.desc = '刘子彬是帅哥'
# p.height = 18.90

# print(p.age,p.name,p.desc)
# 查看对象的所有属性
# print(p.__dict__)

# 4.2 查看一个对象的属性 和修改一个对象的属性
# p.age = 12
# print(p.age)

# p.pets = ['小猫' , '小狗', '小黄']
# print(p.pets , id(p.pets))
#
#
# p.pets.append('小米')
# print(p.pets , id(p.pets))


# 4.3 删除对象属性  del  p.age

# 删除 变量 del  变量名

# print('删除属性之前', p.__dict__)
# del  p.age
# print('删除属性之后',p.__dict__)

# --------------------------------类属性的操作增 、 删除 、 改 、 查---------------------------
class Money:
    _number = 16
    _name = '燕子'
    sex = '男'

# class Test:
#     sex = '女'
# Money.count = 10
# Money.age = 666
# Money.name = '刘子彬的Money'
# Money._name = 'test'
# print('打印Money的对象', Money.__dict__)


# one = Money()
# one._number = 3
# one.__class__ = Test
# print('打印Money的对象', one.__dict__, one.__class__)

# 访问属性流程：先找对象里面是否有这个属性----> 对象的__class__指向的类的属性
# print(one.sex)

# 修改类属性的值：只有通过类.属性值修改 不能通过对象.属性修改

# del Money.sex
# print(Money.__dict__)
# 删除类属性的值：只有通过类.属性值删除 不能通过对象.属性删除

# class Person:
#     age = 10
#
# p = Person()
# p.age += 5
# print('对象的age = %d 类Person的age = %d', p.age, Person.age)


# 限制类的属性 __slots__ 是一个属性限制列表

# class Person:
#     __slots__ = ['age', 'height']
#
#
# p = Person()
# p.age = 10
# p.height = 4
# print(p.age, p.height)














