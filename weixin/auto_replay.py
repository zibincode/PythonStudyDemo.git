#coding=utf-8
# 微信自动回复功能
import itchat
import sys
from weixin.ExcelWriteHelper import *
# 用户模型
class User(object):
    def __init__(self):
        self.__user_name = ""
        self.__nick_name = ""
        self.__head_img_url = ""
        self.__remark_name = ""
        self.__signature = ""
        self.__sex = ""
        self.__city = ""

    @property
    def user_name(self):
        return self.__user_name

    @user_name.setter
    def user_name(self, value):
        self.__user_name = value

    @property
    def nick_name(self):
        return self.__nick_name

    @nick_name.setter
    def nick_name(self, value):
        self.__nick_name = value

    @property
    def head_img_url(self):
        return self.__head_img_url

    @head_img_url.setter
    def head_img_url(self, value):
        self.__head_img_url = value

    @property
    def signature(self):
        return self.__signature

    @signature.setter
    def signature(self, value):
        self.__signature = value

    @property
    def sex(self):
        return self.__sex

    @sex.setter
    def sex(self, value):
        if value == 1:
            self.__sex = '男'
        else:
            self.__sex = '女'

    @property
    def city(self):
        return self.__city

    @city.setter
    def city(self, value):
        self.__city = value

    @property
    def remark_name(self):
        return self.__remark_name

    @remark_name.setter
    def remark_name(self, value):
        self.__remark_name = value

# 定义变量
currentUser = User()  # 当前登录者信息
friendsList = []  # 好友列表

# 处理接受微信的信息
def recive_wechat_message(message):
    replay = u'刘子彬正在使用微信开发功能，所有暂时无法进行交流,可以给他打电话哈'
    return replay


# 0.注册需要监听的数据

# 注册re_msg的意义在于，告诉itchat每次有符合特定条件的消息，itchat要把消息作为参数，去调用re_msg。
# 图片itchat.content.PICTURE
# 语音对应itchat.content.RECORDING
# 名片对应itchat.content.CARD
mesageTypes = [itchat.content.TEXT,   # 文本
               itchat.content.PICTURE,  # 图片
               itchat.content.RECORDING,  # 语音
               itchat.content.MAP,  # 地图
               itchat.content.CARD,  # 名片
               itchat.content.NOTE,  # note
               itchat.content.SHARING,  # 分享
               itchat.content.VIDEO,  # 视频
               ]
@itchat.msg_register(mesageTypes)
def text_replay(msg):
    print(msg)
    replay = u''

    fromUserName = msg['FromUserName']

    # 1.获取接受消息内容
    content = msg['Text']

    # 2.如果消息不是自己发出的有效
    if not fromUserName == currentUser.user_name:
        replay = recive_wechat_message(content)
    else:
        replay = u'自己发出消息自己收'
    return replay


def exportFriends(friendsList):
    print('接受到的好友列表', friendsList)
    sheet_w = ExcelWriteHelper('test_sheet01', None)
    sheet_w.set_style(bold=True)
    title_list = ['姓名', '性别', '备注', '签名']
    sheet_w.inset_list_in_row(title_list, 0, 0)
    __index = 1
    for item in friendsList:
        row = list([item.nick_name, item.sex, item.remark_name, item.signature])
        print('======', row)
        if __index < 100:
            sheet_w.inset_list_in_row(row, __index, 0)
        __index = __index + 1

    sheet_w.save_excel("test_sheet.xls")
    print('-------保存完成')

# 主函数入口
def main():
    # 1. 弹出微信登录二维码，扫描登录网页网微信
    itchat.auto_login(hotReload=True)

    # 2. 获取好友列表
    friends = itchat.get_friends(update=True)
    print(friends)

    # 3.获取好友数据，重新组成好友列表
    for i in range(len(friends)):
        item = friends[i]
        if i == 0:
            currentUser.user_name = item['UserName']
            currentUser.nick_name = item['NickName']
            currentUser.head_img_url = item['HeadImgUrl']
            currentUser.remark_name = item['RemarkName']
            currentUser.signature = item['Signature']
            currentUser.sex = item['Sex']
            currentUser.city = item['City']
        else:
            user = User()
            user.user_name = item['UserName']
            user.nick_name = item['NickName']
            user.head_img_url = item['HeadImgUrl']
            user.remark_name = item['RemarkName']
            user.signature = item['Signature']
            user.sex = item['Sex']
            user.city = item['City']
            friendsList.append(user)

    # 导出好友列表
    exportFriends(friendsList)

    # 4. 开始运行
    itchat.run()

if __name__ == '__main__':
    main()





