# -*- coding: utf-8 -*-

'''

 @作者： 刘子彬
 @描述：利用面向对象的思想写入excel中的内容

'''

import os
import xlwt
class ExcelWriteHelper:

    # 初始化
    def __init__(self, sheet_name, style):
        # 1. 创建存放excel文件的对象
        self.__workBook = xlwt.Workbook()
        # 2. 创建存放excel文件中一张表格的对象
        self.__sheet = self.__workBook.add_sheet(sheet_name, cell_overwrite_ok=True)
        # 3.初始化样式
        if style:
            self.set_style(style.name,
                           style.height,
                           style.bold,
                           style.borders,
                           style.borders_colours)
        else:
            self.__style = xlwt.XFStyle()
            print('-------初始化样式None')

    # 设置单元格样式
    def set_style(self, name='Arial',
                  height=400,
                  bold=False,
                  borders=True,
                  borders_colours=0x40):
        # 初始化样式
         self.__style = xlwt.XFStyle()

        # 字体设置
         __font = xlwt.Font()  # 为样式创建字体
         __font.name = name
         __font.height = height
         __font.bold = bold
         self.__style.font = __font

        # 设置边框
         if borders is True:
            __border = xlwt.Borders()
            __border.left = xlwt.Borders.MEDIUM
            __border.right = xlwt.Borders.MEDIUM
            __border.top = xlwt.Borders.MEDIUM
            __border.bottom = xlwt.Borders.MEDIUM
            __border.left_colour = borders_colours
            __border.right_colour = borders_colours
            __border.top_colour = borders_colours
            __border.bottom_colour = borders_colours
            self.__style.borders = __border

    def save_excel(self, path):
        '''
        保存表格
        :param path: 保存的路径
        :return:
        '''
        __full_path = None
        if path:
            __full_path = path
        else:
            __full_path = ''  # 默认的路径
        # 保存数据
        self.__workBook.save(__full_path)

    def inset_value_in_cell(self, value, row_index, col_index):
        '''
         把字符串插入表格的某一个单元
        :param value:  插入的值
        :param row_index:  行号
        :param col_index:  列号
        :return:
        '''
        self.__sheet.write(row_index, col_index, value, self.__style)

    def inset_list_in_row(self, value, row_start_index, col_start_index):
        '''
        把列表填入表格的行
        :param value: 列表值
        :param row_start_index:  填入列表所在的起始行
        :param col_start_index:  填入列表所在的起始列
        :return:
        '''
        __row_index = 0
        __col_index = 0
        if row_start_index:
            __row_index = row_start_index
        else:
            __row_index = 0

        if col_start_index:
            __col_index = col_start_index
        else:
            __col_index = 0

        for __item in value:
            self.inset_value_in_cell(__item, __row_index, __col_index)
            __col_index = __col_index + 1

    def inset_list_in_col(self, value, row_start_index, col_start_index):
        '''
          把列表填入表格的列
        :param value:  数组列表值
        :param row_start_index:  填入列表所在的起始行
        :param col_start_index:  填入列表所在的起始列
        :return:
        '''
        __row_index = 0
        __col_index = 0
        if row_start_index:
            __row_index = row_start_index
        else:
            __row_index = 0

        if col_start_index:
            __col_index = col_start_index
        else:
            __col_index = 0

        for __item in value:
            self.inset_value_in_cell(__item, __row_index, __col_index)
            __row_index = __row_index + 1

    def inset_list_in_area(self, value, row_start_index, col_start_index):
        '''
        把列表填入区域 - 按顺序填入表格
        :param value:
        :param row_start_index:
        :param col_start_index:
        :return:
        '''
        __row_index = row_start_index
        __col_index = col_start_index
        for item_list in value:
            for item in item_list:
                self.inset_value_in_cell(item, __row_index, __col_index)
                __col_index = __col_index + 1
            # 换行
            __col_index = row_start_index
            __row_index = __row_index + 1















